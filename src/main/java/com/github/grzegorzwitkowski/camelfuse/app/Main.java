package com.github.grzegorzwitkowski.camelfuse.app;

import org.apache.camel.CamelContext;
import org.apache.camel.spring.SpringCamelContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext springContext = new ClassPathXmlApplicationContext("META-INF/spring/applicationContext.xml");
        CamelContext camelContext = new SpringCamelContext(springContext);
        camelContext.start();

        TimeUnit.SECONDS.sleep(3);

        camelContext.stop();
    }

}
