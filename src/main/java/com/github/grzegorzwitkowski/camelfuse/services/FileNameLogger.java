package com.github.grzegorzwitkowski.camelfuse.services;

import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FileNameLogger {

    private final AuditLogger auditLogger;

    @Autowired
    public FileNameLogger(AuditLogger auditLogger) {
        this.auditLogger = auditLogger;
    }

    public void logFileName(Exchange exchange) {
        Object fileName = exchange.getIn().getHeader("CamelFileName");
        auditLogger.log(String.format("copying file %s", fileName));
    }
}
