package com.github.grzegorzwitkowski.camelfuse.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AuditLogger {

    private static Logger log = LoggerFactory.getLogger(AuditLogger.class);

    public void log(String message) {
        log.info(">>> " + message);
    }
}
