package com.github.grzegorzwitkowski.camelfuse.routes;

import com.github.grzegorzwitkowski.camelfuse.services.FileNameLogger;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class FileRoute extends RouteBuilder {

    private final FileNameLogger fileNameLogger;

    @Autowired
    public FileRoute(FileNameLogger fileNameLogger) {
        this.fileNameLogger = fileNameLogger;
    }

    @Override
    public void configure() throws Exception {
        from("file:C:/Users/gwitkowski/Desktop/camel-fuse/in?noop=true")
                .bean(fileNameLogger)
                .to("file:C:/Users/gwitkowski/Desktop/camel-fuse/out");
    }
}
